

package ficheros;

import java.io.*;
import java.util.*;

/**
 *
 * @author pcelardperez
 */
public class LerFich {
    
    
    //método para ler dentro do ficheiro (recibe un obxecto de tipo file)
    //daranos unha excepcion. Podo lanzala ou capturala  
    public static void lerPalabras(File ficheiro) {
        //Abro o fluxo para ler:
        Scanner sc = null;//declaramos Scanner e inicializámolo a null
        try {
            sc = new Scanner(ficheiro);//creamos obxecto tipo scanner que colla info do noso ficheiro. 

            //Leo ata o final do ficheiro    
            while (sc.hasNext()) {            //mentres haiba algo que ler
                String dato = sc.next();    //Leoo e recolloo nun String
                System.out.println(dato);   //Visualizo o dato
            }
        } catch (FileNotFoundException ex) {
            System.out.println("erro1" + ex.getMessage());
        } finally {//sempre teño que fechar o ficheiro (comprobo que o haxa aberto antes)
            //Pecho o fluxo 
            if (sc != null) {
                sc.close();
            }
        }
    }

    public static void lerNumeros(File ficheiro) {
        //Abro o fluxo para ler:
        Scanner sc = null;//declaramos Scanner e inicializámolo a null
        try {
            sc = new Scanner(ficheiro);//creamos obxecto tipo scanner que colla info do noso ficheiro. 

            //Leo ata o final do ficheiro    
            while (sc.hasNext()) {            //mentres haiba algo que ler
                int dato = sc.nextInt();    //Leoo e recolloo nun int
                System.out.println(dato);   //Visualizo o dato
            }
        } catch (IOException ex) {
            System.out.println("erro2" + ex.getMessage());
        } finally {//sempre teño que fechar o ficheiro (comprobo que o haxa aberto antes)
            //Pecho o fluxo 
            if (sc != null) {
                sc.close();
            }
        }

    }

    public static void lerSeparadores(File ficheiro){
        Scanner sc = null; //Declaramos un objeto de tipo Scanner para leer.
        try {
            sc = new Scanner(ficheiro).useDelimiter(",");   /*Se le dice donde tendrá que leer en vez de por teclado,
                                                            en este caso "ficheiro", si no se pone.useDelimeter(","); la frase por 
                                                            ejemplo un,dos,tres sería un único String*/
            while(sc.hasNext()){  /* Mientras tenga algo que leer...*/
               String num = sc.next();     /* ...leo. */
                System.out.println(num);
            }
        } catch (FileNotFoundException ex) {
            System.out.println(" erro 5 "+ex.getMessage());
        }finally{
            if(sc!=null)
                sc.close();
        }
    }
    
    public static void lerObxectos(File ficheiro){
        Scanner sc = null;
        ArrayList<Persoa> lista = new ArrayList<Persoa>(); /* Creamos un ArrayList donde meter los datos */
        
        try{
            sc = new Scanner(ficheiro);
            while(sc.hasNext()){
                String cadea = sc.nextLine();
                String datos[]=cadea.split(","); /* .split coge una cadena de String y devuelve un Array de String 
                                                    se pone "," para decir que es lo que separa.*/
                Persoa p = new Persoa(datos[0],datos[1],Integer.parseInt(datos[2]));    
                lista.add(p);
            }
            
            
        }catch(FileNotFoundException ex){
            System.out.println(" erro 6 "+ex.getMessage());
        }finally{
            if(sc!=null)
                sc.close();
        }
        for(int i=0;i<lista.size();i++){
            System.out.println(lista.get(i)); /* Visualizamos */
        }
        
    }
    public static void lerObxectosSenSeparador(File ficheiro){
        Scanner sc = null;
            try{
                sc=new Scanner(ficheiro);
                while(sc.hasNext()){
                    String aux = sc.nextLine();
                    System.out.println(aux);
                }
            }catch(IOException ex){
                System.out.println("erro de lectura" + ex.getMessage());
            }finally {
                if (sc != null) {
                sc.close();
            }
        }    
    }
 }
