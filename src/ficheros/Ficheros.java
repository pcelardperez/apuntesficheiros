

package ficheros;

import java.io.*;

/**
 *
 * @author pcelardperez
 */

//o arquivo gardámolo na ruta: Z:\Programación\2º Avaliación\Apuntes\Ficheiros\Ficheros\palabras.txt

public class Ficheros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Creamos ficheiro de tipo file (podo darlle o nome do ficheiro ou coa ruta)    

        //File ficheiro = new File("palabras.txt");//co nome
        File ficheiro=new File("Z:\\Programación\\2º Avaliación\\Apuntes\\Ficheiros\\Ficheros\\palabras.txt");//coa ruta
        LerFich.lerPalabras(ficheiro);
        File ficheiro2=new File("numeros.txt");
        LerFich.lerNumeros(ficheiro2);
        File ficheiro3 = new File("separadores.txt");
        LerFich.lerSeparadores(ficheiro3);
        File ficheiro4 = new File("datos.txt");
        LerFich.lerObxectos(ficheiro4);
        /* Escribimos o noso ficheiro */
        File ficheiro5 = new File("persoas.txt");
        EscribirFicheiro.escribir(ficheiro5);
        File ficheiro6 = new File("persoas.txt");
        LerFich.lerObxectosSenSeparador(ficheiro6);
        File ficheiro7 = new File("IntroducirPersoas.txt");
        EngadirDatos.introducirPersoas(ficheiro7);
        LerFich.lerObxectosSenSeparador(ficheiro7);
    }
    
}
