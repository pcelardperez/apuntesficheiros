/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ficheros;

/**
 *
 * @author pcelardperez
 */
public class Persoa {
    private String nome;
    private String dni;
    private int edade;
    
    public Persoa(){
        
    }
    public Persoa(String nome, String dni, int edade){
        this.nome=nome;
        this.dni=dni;
        this.edade=edade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getEdade() {
        return edade;
    }

    public void setEdade(int edade) {
        this.edade = edade;
    }

    @Override /* Boton derecho -> insert code -> toString() */ 
    public String toString() {
        return "Persoa{" + "nome=" + nome + ", dni=" + dni + ", edade=" + edade + '}';
    }
}

