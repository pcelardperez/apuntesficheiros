/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ficheros;

import java.io.*;
import java.util.Scanner;

/**
 *
 * @author pcelardperez
 */
public class EscribirFicheiro {
    
    public static void escribir(File ficheiro){
        PrintWriter f = null;
        Persoa p = new Persoa("ass","999",90);
        Persoa p2= new Persoa("uiy","888",60);
        try{
            f = new PrintWriter(ficheiro); /* Abro ficheiro para escribir */
            f.println(p.getNome()+", "+p.getDni()+", "+p.getEdade());  /* Escribo no ficheiro */
            f.println(p2); /* Escribo os datos da segunda persoa*/
        }catch(FileNotFoundException e){
            System.out.println("erro de escritura"+e.getMessage());
        }finally{
            if(f != null)
                f.close();  /* Pechamos ficheiro */
        }
    }
    
}
